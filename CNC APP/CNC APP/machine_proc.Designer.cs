﻿namespace CNC_APP
{
    partial class machine_proc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadTool = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Preseting_Mode = new System.Windows.Forms.ComboBox();
            this.comboBox_Tool_Type = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_Tool_number = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_Tool_diameter = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Tool_Length = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_Max_Consumption = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_Presetting = new System.Windows.Forms.Button();
            this.btn_Home = new System.Windows.Forms.Button();
            this.btn_warm_up = new System.Windows.Forms.Button();
            this.btn_Close_Rack = new System.Windows.Forms.Button();
            this.btn_Open_Rack = new System.Windows.Forms.Button();
            this.btn_Drive_Off = new System.Windows.Forms.Button();
            this.btn_Drive_On = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(23, 36);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(691, 120);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Message Log:";
            // 
            // LoadTool
            // 
            this.LoadTool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LoadTool.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LoadTool.Location = new System.Drawing.Point(432, 197);
            this.LoadTool.Name = "LoadTool";
            this.LoadTool.Size = new System.Drawing.Size(106, 58);
            this.LoadTool.TabIndex = 2;
            this.LoadTool.Text = "Load Tool";
            this.LoadTool.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox_Max_Consumption);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox_Tool_Length);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_Tool_diameter);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_Tool_number);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBox_Tool_Type);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBox_Preseting_Mode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(14, 186);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(391, 420);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tool info";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 60);
            this.label2.TabIndex = 3;
            this.label2.Text = "1.Select the preseting mode\r\n(0=Automatic,1-Manual ):\r\n\r\n";
            // 
            // comboBox_Preseting_Mode
            // 
            this.comboBox_Preseting_Mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Preseting_Mode.FormattingEnabled = true;
            this.comboBox_Preseting_Mode.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBox_Preseting_Mode.Location = new System.Drawing.Point(264, 52);
            this.comboBox_Preseting_Mode.Name = "comboBox_Preseting_Mode";
            this.comboBox_Preseting_Mode.Size = new System.Drawing.Size(99, 28);
            this.comboBox_Preseting_Mode.TabIndex = 4;
            // 
            // comboBox_Tool_Type
            // 
            this.comboBox_Tool_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Tool_Type.FormattingEnabled = true;
            this.comboBox_Tool_Type.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboBox_Tool_Type.Location = new System.Drawing.Point(264, 155);
            this.comboBox_Tool_Type.Name = "comboBox_Tool_Type";
            this.comboBox_Tool_Type.Size = new System.Drawing.Size(99, 28);
            this.comboBox_Tool_Type.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 40);
            this.label3.TabIndex = 5;
            this.label3.Text = "3.Select the type of tool\r\n(1=spherical,2=cylindrical,3=disk):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "2.Select the tool number:";
            // 
            // textBox_Tool_number
            // 
            this.textBox_Tool_number.Location = new System.Drawing.Point(264, 100);
            this.textBox_Tool_number.Name = "textBox_Tool_number";
            this.textBox_Tool_number.Size = new System.Drawing.Size(100, 26);
            this.textBox_Tool_number.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(217, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "4.insert the tool diameter(mm):";
            // 
            // textBox_Tool_diameter
            // 
            this.textBox_Tool_diameter.Location = new System.Drawing.Point(263, 210);
            this.textBox_Tool_diameter.Name = "textBox_Tool_diameter";
            this.textBox_Tool_diameter.Size = new System.Drawing.Size(100, 26);
            this.textBox_Tool_diameter.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(201, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "5.insert the tool length(mm):";
            // 
            // textBox_Tool_Length
            // 
            this.textBox_Tool_Length.Location = new System.Drawing.Point(263, 264);
            this.textBox_Tool_Length.Name = "textBox_Tool_Length";
            this.textBox_Tool_Length.Size = new System.Drawing.Size(100, 26);
            this.textBox_Tool_Length.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(238, 40);
            this.label7.TabIndex = 13;
            this.label7.Text = "6.Maximum allowed consumption\r\n(only for automatic prset)[mm]:";
            // 
            // textBox_Max_Consumption
            // 
            this.textBox_Max_Consumption.Location = new System.Drawing.Point(263, 318);
            this.textBox_Max_Consumption.Name = "textBox_Max_Consumption";
            this.textBox_Max_Consumption.Size = new System.Drawing.Size(100, 26);
            this.textBox_Max_Consumption.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(22, 374);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(336, 40);
            this.button1.TabIndex = 15;
            this.button1.Text = "change Tool Info";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btn_Presetting
            // 
            this.btn_Presetting.BackColor = System.Drawing.Color.Fuchsia;
            this.btn_Presetting.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Presetting.Location = new System.Drawing.Point(576, 197);
            this.btn_Presetting.Name = "btn_Presetting";
            this.btn_Presetting.Size = new System.Drawing.Size(106, 58);
            this.btn_Presetting.TabIndex = 4;
            this.btn_Presetting.Text = "Presetting";
            this.btn_Presetting.UseVisualStyleBackColor = false;
            // 
            // btn_Home
            // 
            this.btn_Home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_Home.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Home.Location = new System.Drawing.Point(432, 274);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(106, 58);
            this.btn_Home.TabIndex = 5;
            this.btn_Home.Text = "Home";
            this.btn_Home.UseVisualStyleBackColor = false;
            // 
            // btn_warm_up
            // 
            this.btn_warm_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_warm_up.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_warm_up.Location = new System.Drawing.Point(576, 274);
            this.btn_warm_up.Name = "btn_warm_up";
            this.btn_warm_up.Size = new System.Drawing.Size(106, 58);
            this.btn_warm_up.TabIndex = 6;
            this.btn_warm_up.Text = "Warm up Spindle";
            this.btn_warm_up.UseVisualStyleBackColor = false;
            // 
            // btn_Close_Rack
            // 
            this.btn_Close_Rack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_Close_Rack.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close_Rack.Location = new System.Drawing.Point(576, 364);
            this.btn_Close_Rack.Name = "btn_Close_Rack";
            this.btn_Close_Rack.Size = new System.Drawing.Size(106, 58);
            this.btn_Close_Rack.TabIndex = 8;
            this.btn_Close_Rack.Text = "Close Rack";
            this.btn_Close_Rack.UseVisualStyleBackColor = false;
            // 
            // btn_Open_Rack
            // 
            this.btn_Open_Rack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_Open_Rack.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Open_Rack.Location = new System.Drawing.Point(432, 364);
            this.btn_Open_Rack.Name = "btn_Open_Rack";
            this.btn_Open_Rack.Size = new System.Drawing.Size(106, 58);
            this.btn_Open_Rack.TabIndex = 7;
            this.btn_Open_Rack.Text = "Open Rack";
            this.btn_Open_Rack.UseVisualStyleBackColor = false;
            // 
            // btn_Drive_Off
            // 
            this.btn_Drive_Off.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btn_Drive_Off.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Drive_Off.Location = new System.Drawing.Point(576, 450);
            this.btn_Drive_Off.Name = "btn_Drive_Off";
            this.btn_Drive_Off.Size = new System.Drawing.Size(106, 58);
            this.btn_Drive_Off.TabIndex = 10;
            this.btn_Drive_Off.Text = "Drive Off";
            this.btn_Drive_Off.UseVisualStyleBackColor = false;
            // 
            // btn_Drive_On
            // 
            this.btn_Drive_On.BackColor = System.Drawing.Color.White;
            this.btn_Drive_On.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Drive_On.Location = new System.Drawing.Point(432, 450);
            this.btn_Drive_On.Name = "btn_Drive_On";
            this.btn_Drive_On.Size = new System.Drawing.Size(106, 58);
            this.btn_Drive_On.TabIndex = 9;
            this.btn_Drive_On.Text = "Drive On";
            this.btn_Drive_On.UseVisualStyleBackColor = false;
            // 
            // btn_Exit
            // 
            this.btn_Exit.BackColor = System.Drawing.Color.Yellow;
            this.btn_Exit.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Exit.Location = new System.Drawing.Point(424, 560);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(277, 40);
            this.btn_Exit.TabIndex = 11;
            this.btn_Exit.Text = "Exit";
            this.btn_Exit.UseVisualStyleBackColor = false;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // machine_proc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 618);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_Drive_Off);
            this.Controls.Add(this.btn_Drive_On);
            this.Controls.Add(this.btn_Close_Rack);
            this.Controls.Add(this.btn_Open_Rack);
            this.Controls.Add(this.btn_warm_up);
            this.Controls.Add(this.btn_Home);
            this.Controls.Add(this.btn_Presetting);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.LoadTool);
            this.Name = "machine_proc";
            this.Text = "Machine Procedure";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadTool;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_Max_Consumption;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_Tool_Length;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Tool_diameter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_Tool_number;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_Tool_Type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_Preseting_Mode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_Presetting;
        private System.Windows.Forms.Button btn_Home;
        private System.Windows.Forms.Button btn_warm_up;
        private System.Windows.Forms.Button btn_Close_Rack;
        private System.Windows.Forms.Button btn_Open_Rack;
        private System.Windows.Forms.Button btn_Drive_Off;
        private System.Windows.Forms.Button btn_Drive_On;
        private System.Windows.Forms.Button btn_Exit;
    }
}