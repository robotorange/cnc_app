﻿namespace CNC_APP
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ABB_CONNECT = new System.Windows.Forms.Button();
            this.EXIT = new System.Windows.Forms.Button();
            this.MESSAGE_LOG = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.YKSRC = new System.Windows.Forms.Button();
            this.File_Manager = new System.Windows.Forms.Button();
            this.Procedure_debug = new System.Windows.Forms.Button();
            this.Debug = new System.Windows.Forms.Button();
            this.Timer = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox_LOGO = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LOGO)).BeginInit();
            this.SuspendLayout();
            // 
            // ABB_CONNECT
            // 
            this.ABB_CONNECT.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ABB_CONNECT.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ABB_CONNECT.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ABB_CONNECT.Location = new System.Drawing.Point(40, 384);
            this.ABB_CONNECT.Margin = new System.Windows.Forms.Padding(2);
            this.ABB_CONNECT.Name = "ABB_CONNECT";
            this.ABB_CONNECT.Size = new System.Drawing.Size(131, 48);
            this.ABB_CONNECT.TabIndex = 0;
            this.ABB_CONNECT.Text = "ABB Connection";
            this.ABB_CONNECT.UseVisualStyleBackColor = false;
            this.ABB_CONNECT.Click += new System.EventHandler(this.ABB_CONNECT_Click);
            // 
            // EXIT
            // 
            this.EXIT.BackColor = System.Drawing.Color.Yellow;
            this.EXIT.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.EXIT.Location = new System.Drawing.Point(411, 384);
            this.EXIT.Margin = new System.Windows.Forms.Padding(2);
            this.EXIT.Name = "EXIT";
            this.EXIT.Size = new System.Drawing.Size(131, 48);
            this.EXIT.TabIndex = 1;
            this.EXIT.Text = "EXIT";
            this.EXIT.UseVisualStyleBackColor = false;
            this.EXIT.Click += new System.EventHandler(this.EXIT_Click);
            // 
            // MESSAGE_LOG
            // 
            this.MESSAGE_LOG.Location = new System.Drawing.Point(32, 266);
            this.MESSAGE_LOG.Margin = new System.Windows.Forms.Padding(2);
            this.MESSAGE_LOG.Multiline = true;
            this.MESSAGE_LOG.Name = "MESSAGE_LOG";
            this.MESSAGE_LOG.Size = new System.Drawing.Size(507, 100);
            this.MESSAGE_LOG.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(29, 244);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Message Log";
            // 
            // YKSRC
            // 
            this.YKSRC.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.YKSRC.Location = new System.Drawing.Point(294, 159);
            this.YKSRC.Name = "YKSRC";
            this.YKSRC.Size = new System.Drawing.Size(120, 57);
            this.YKSRC.TabIndex = 4;
            this.YKSRC.Text = "Machining";
            this.YKSRC.UseVisualStyleBackColor = true;
            this.YKSRC.Click += new System.EventHandler(this.YKSRC_Click);
            // 
            // File_Manager
            // 
            this.File_Manager.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.File_Manager.Location = new System.Drawing.Point(159, 159);
            this.File_Manager.Name = "File_Manager";
            this.File_Manager.Size = new System.Drawing.Size(117, 57);
            this.File_Manager.TabIndex = 5;
            this.File_Manager.Text = "File manager";
            this.File_Manager.UseVisualStyleBackColor = true;
            this.File_Manager.Click += new System.EventHandler(this.File_Manager_Click);
            // 
            // Procedure_debug
            // 
            this.Procedure_debug.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Procedure_debug.Location = new System.Drawing.Point(32, 159);
            this.Procedure_debug.Name = "Procedure_debug";
            this.Procedure_debug.Size = new System.Drawing.Size(109, 57);
            this.Procedure_debug.TabIndex = 6;
            this.Procedure_debug.Text = "Machine Procedure";
            this.Procedure_debug.UseVisualStyleBackColor = true;
            this.Procedure_debug.Click += new System.EventHandler(this.button1_Click);
            // 
            // Debug
            // 
            this.Debug.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Debug.Location = new System.Drawing.Point(432, 159);
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(110, 57);
            this.Debug.TabIndex = 7;
            this.Debug.Text = "Robot Debug";
            this.Debug.UseVisualStyleBackColor = true;
            this.Debug.Click += new System.EventHandler(this.Debug_Click);
            // 
            // Timer
            // 
            this.Timer.AutoSize = true;
            this.Timer.Location = new System.Drawing.Point(360, 438);
            this.Timer.Name = "Timer";
            this.Timer.Size = new System.Drawing.Size(41, 12);
            this.Timer.TabIndex = 8;
            this.Timer.Text = "label2";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox_LOGO
            // 
            this.pictureBox_LOGO.Location = new System.Drawing.Point(32, 12);
            this.pictureBox_LOGO.Name = "pictureBox_LOGO";
            this.pictureBox_LOGO.Size = new System.Drawing.Size(507, 137);
            this.pictureBox_LOGO.TabIndex = 9;
            this.pictureBox_LOGO.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 472);
            this.Controls.Add(this.pictureBox_LOGO);
            this.Controls.Add(this.Timer);
            this.Controls.Add(this.Debug);
            this.Controls.Add(this.Procedure_debug);
            this.Controls.Add(this.File_Manager);
            this.Controls.Add(this.YKSRC);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MESSAGE_LOG);
            this.Controls.Add(this.EXIT);
            this.Controls.Add(this.ABB_CONNECT);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "ABB CNC";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LOGO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ABB_CONNECT;
        private System.Windows.Forms.Button EXIT;
        private System.Windows.Forms.TextBox MESSAGE_LOG;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button YKSRC;
        private System.Windows.Forms.Button File_Manager;
        private System.Windows.Forms.Button Procedure_debug;
        private System.Windows.Forms.Button Debug;
        private System.Windows.Forms.Label Timer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox_LOGO;
    }
}

