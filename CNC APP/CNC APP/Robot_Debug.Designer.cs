﻿namespace CNC_APP
{
    partial class Robot_Debug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_x = new System.Windows.Forms.TextBox();
            this.label_x = new System.Windows.Forms.Label();
            this.label_Y = new System.Windows.Forms.Label();
            this.textBox_y = new System.Windows.Forms.TextBox();
            this.label_q0 = new System.Windows.Forms.Label();
            this.textBox_q0 = new System.Windows.Forms.TextBox();
            this.label_z = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label_q1 = new System.Windows.Forms.Label();
            this.textBox_q1 = new System.Windows.Forms.TextBox();
            this.label1_q2 = new System.Windows.Forms.Label();
            this.textBox_q2 = new System.Windows.Forms.TextBox();
            this.label_q3 = new System.Windows.Forms.Label();
            this.textBox_q3 = new System.Windows.Forms.TextBox();
            this.label_J6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label_J5 = new System.Windows.Forms.Label();
            this.textBox_J5 = new System.Windows.Forms.TextBox();
            this.label_J4 = new System.Windows.Forms.Label();
            this.textBox_J4 = new System.Windows.Forms.TextBox();
            this.label_J3 = new System.Windows.Forms.Label();
            this.textBox_J3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_J2 = new System.Windows.Forms.TextBox();
            this.J1 = new System.Windows.Forms.Label();
            this.textBox_J1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_Robot_IP = new System.Windows.Forms.Label();
            this.textBox_Robot_IP = new System.Windows.Forms.TextBox();
            this.textBox_PC_IP = new System.Windows.Forms.TextBox();
            this.label_PC_IP = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label_MOTION_PORT = new System.Windows.Forms.Label();
            this.ratio_sim = new System.Windows.Forms.RadioButton();
            this.radio_Real = new System.Windows.Forms.RadioButton();
            this.connect_robot = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioAbs = new System.Windows.Forms.RadioButton();
            this.radioRel = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_AxisNo = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_Tool_Axis = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonMoveZero = new System.Windows.Forms.Button();
            this.buttonSetIO = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_x
            // 
            this.textBox_x.Location = new System.Drawing.Point(62, 35);
            this.textBox_x.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_x.Name = "textBox_x";
            this.textBox_x.Size = new System.Drawing.Size(101, 26);
            this.textBox_x.TabIndex = 0;
            // 
            // label_x
            // 
            this.label_x.AutoSize = true;
            this.label_x.Location = new System.Drawing.Point(21, 42);
            this.label_x.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_x.Name = "label_x";
            this.label_x.Size = new System.Drawing.Size(21, 20);
            this.label_x.TabIndex = 1;
            this.label_x.Text = "X:";
            // 
            // label_Y
            // 
            this.label_Y.AutoSize = true;
            this.label_Y.Location = new System.Drawing.Point(21, 81);
            this.label_Y.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Y.Name = "label_Y";
            this.label_Y.Size = new System.Drawing.Size(20, 20);
            this.label_Y.TabIndex = 3;
            this.label_Y.Text = "Y:";
            // 
            // textBox_y
            // 
            this.textBox_y.Location = new System.Drawing.Point(62, 77);
            this.textBox_y.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_y.Name = "textBox_y";
            this.textBox_y.Size = new System.Drawing.Size(101, 26);
            this.textBox_y.TabIndex = 2;
            // 
            // label_q0
            // 
            this.label_q0.AutoSize = true;
            this.label_q0.Location = new System.Drawing.Point(19, 168);
            this.label_q0.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_q0.Name = "label_q0";
            this.label_q0.Size = new System.Drawing.Size(29, 20);
            this.label_q0.TabIndex = 7;
            this.label_q0.Text = "q0:";
            // 
            // textBox_q0
            // 
            this.textBox_q0.Location = new System.Drawing.Point(62, 162);
            this.textBox_q0.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_q0.Name = "textBox_q0";
            this.textBox_q0.Size = new System.Drawing.Size(101, 26);
            this.textBox_q0.TabIndex = 6;
            // 
            // label_z
            // 
            this.label_z.AutoSize = true;
            this.label_z.Location = new System.Drawing.Point(21, 122);
            this.label_z.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_z.Name = "label_z";
            this.label_z.Size = new System.Drawing.Size(21, 20);
            this.label_z.TabIndex = 5;
            this.label_z.Text = "Z:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(62, 120);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(101, 26);
            this.textBox3.TabIndex = 4;
            // 
            // label_q1
            // 
            this.label_q1.AutoSize = true;
            this.label_q1.Location = new System.Drawing.Point(19, 213);
            this.label_q1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_q1.Name = "label_q1";
            this.label_q1.Size = new System.Drawing.Size(29, 20);
            this.label_q1.TabIndex = 9;
            this.label_q1.Text = "q1:";
            // 
            // textBox_q1
            // 
            this.textBox_q1.Location = new System.Drawing.Point(62, 207);
            this.textBox_q1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_q1.Name = "textBox_q1";
            this.textBox_q1.Size = new System.Drawing.Size(101, 26);
            this.textBox_q1.TabIndex = 8;
            // 
            // label1_q2
            // 
            this.label1_q2.AutoSize = true;
            this.label1_q2.Location = new System.Drawing.Point(19, 257);
            this.label1_q2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1_q2.Name = "label1_q2";
            this.label1_q2.Size = new System.Drawing.Size(29, 20);
            this.label1_q2.TabIndex = 11;
            this.label1_q2.Text = "q2:";
            // 
            // textBox_q2
            // 
            this.textBox_q2.Location = new System.Drawing.Point(62, 252);
            this.textBox_q2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_q2.Name = "textBox_q2";
            this.textBox_q2.Size = new System.Drawing.Size(101, 26);
            this.textBox_q2.TabIndex = 10;
            // 
            // label_q3
            // 
            this.label_q3.AutoSize = true;
            this.label_q3.Location = new System.Drawing.Point(19, 303);
            this.label_q3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_q3.Name = "label_q3";
            this.label_q3.Size = new System.Drawing.Size(29, 20);
            this.label_q3.TabIndex = 13;
            this.label_q3.Text = "q3:";
            // 
            // textBox_q3
            // 
            this.textBox_q3.Location = new System.Drawing.Point(62, 296);
            this.textBox_q3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_q3.Name = "textBox_q3";
            this.textBox_q3.Size = new System.Drawing.Size(101, 26);
            this.textBox_q3.TabIndex = 12;
            // 
            // label_J6
            // 
            this.label_J6.AutoSize = true;
            this.label_J6.Location = new System.Drawing.Point(209, 292);
            this.label_J6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_J6.Name = "label_J6";
            this.label_J6.Size = new System.Drawing.Size(26, 20);
            this.label_J6.TabIndex = 25;
            this.label_J6.Text = "J6:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(252, 295);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(101, 26);
            this.textBox1.TabIndex = 24;
            // 
            // label_J5
            // 
            this.label_J5.AutoSize = true;
            this.label_J5.Location = new System.Drawing.Point(209, 245);
            this.label_J5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_J5.Name = "label_J5";
            this.label_J5.Size = new System.Drawing.Size(26, 20);
            this.label_J5.TabIndex = 23;
            this.label_J5.Text = "J5:";
            // 
            // textBox_J5
            // 
            this.textBox_J5.Location = new System.Drawing.Point(252, 242);
            this.textBox_J5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_J5.Name = "textBox_J5";
            this.textBox_J5.Size = new System.Drawing.Size(101, 26);
            this.textBox_J5.TabIndex = 22;
            // 
            // label_J4
            // 
            this.label_J4.AutoSize = true;
            this.label_J4.Location = new System.Drawing.Point(209, 192);
            this.label_J4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_J4.Name = "label_J4";
            this.label_J4.Size = new System.Drawing.Size(26, 20);
            this.label_J4.TabIndex = 21;
            this.label_J4.Text = "J4:";
            // 
            // textBox_J4
            // 
            this.textBox_J4.Location = new System.Drawing.Point(252, 190);
            this.textBox_J4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_J4.Name = "textBox_J4";
            this.textBox_J4.Size = new System.Drawing.Size(101, 26);
            this.textBox_J4.TabIndex = 20;
            // 
            // label_J3
            // 
            this.label_J3.AutoSize = true;
            this.label_J3.Location = new System.Drawing.Point(211, 144);
            this.label_J3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_J3.Name = "label_J3";
            this.label_J3.Size = new System.Drawing.Size(26, 20);
            this.label_J3.TabIndex = 19;
            this.label_J3.Text = "J3:";
            // 
            // textBox_J3
            // 
            this.textBox_J3.Location = new System.Drawing.Point(252, 140);
            this.textBox_J3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_J3.Name = "textBox_J3";
            this.textBox_J3.Size = new System.Drawing.Size(101, 26);
            this.textBox_J3.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(211, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "J2:";
            // 
            // textBox_J2
            // 
            this.textBox_J2.Location = new System.Drawing.Point(252, 89);
            this.textBox_J2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_J2.Name = "textBox_J2";
            this.textBox_J2.Size = new System.Drawing.Size(101, 26);
            this.textBox_J2.TabIndex = 16;
            // 
            // J1
            // 
            this.J1.AutoSize = true;
            this.J1.Location = new System.Drawing.Point(211, 45);
            this.J1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.J1.Name = "J1";
            this.J1.Size = new System.Drawing.Size(26, 20);
            this.J1.TabIndex = 15;
            this.J1.Text = "J1:";
            // 
            // textBox_J1
            // 
            this.textBox_J1.Location = new System.Drawing.Point(252, 38);
            this.textBox_J1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_J1.Name = "textBox_J1";
            this.textBox_J1.Size = new System.Drawing.Size(101, 26);
            this.textBox_J1.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_J6);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label_J5);
            this.groupBox1.Controls.Add(this.textBox_J5);
            this.groupBox1.Controls.Add(this.label_J4);
            this.groupBox1.Controls.Add(this.textBox_J4);
            this.groupBox1.Controls.Add(this.label_J3);
            this.groupBox1.Controls.Add(this.textBox_J3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_J2);
            this.groupBox1.Controls.Add(this.J1);
            this.groupBox1.Controls.Add(this.textBox_J1);
            this.groupBox1.Controls.Add(this.label_q3);
            this.groupBox1.Controls.Add(this.textBox_q3);
            this.groupBox1.Controls.Add(this.label1_q2);
            this.groupBox1.Controls.Add(this.textBox_q2);
            this.groupBox1.Controls.Add(this.label_q1);
            this.groupBox1.Controls.Add(this.textBox_q1);
            this.groupBox1.Controls.Add(this.label_q0);
            this.groupBox1.Controls.Add(this.textBox_q0);
            this.groupBox1.Controls.Add(this.label_z);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label_Y);
            this.groupBox1.Controls.Add(this.textBox_y);
            this.groupBox1.Controls.Add(this.label_x);
            this.groupBox1.Controls.Add(this.textBox_x);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(12, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(391, 344);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Robot state";
            // 
            // label_Robot_IP
            // 
            this.label_Robot_IP.AutoSize = true;
            this.label_Robot_IP.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_Robot_IP.Location = new System.Drawing.Point(31, 30);
            this.label_Robot_IP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Robot_IP.Name = "label_Robot_IP";
            this.label_Robot_IP.Size = new System.Drawing.Size(87, 20);
            this.label_Robot_IP.TabIndex = 27;
            this.label_Robot_IP.Text = "Robot IP：";
            // 
            // textBox_Robot_IP
            // 
            this.textBox_Robot_IP.Location = new System.Drawing.Point(151, 26);
            this.textBox_Robot_IP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_Robot_IP.Name = "textBox_Robot_IP";
            this.textBox_Robot_IP.Size = new System.Drawing.Size(137, 26);
            this.textBox_Robot_IP.TabIndex = 28;
            // 
            // textBox_PC_IP
            // 
            this.textBox_PC_IP.Location = new System.Drawing.Point(151, 79);
            this.textBox_PC_IP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_PC_IP.Name = "textBox_PC_IP";
            this.textBox_PC_IP.Size = new System.Drawing.Size(137, 26);
            this.textBox_PC_IP.TabIndex = 30;
            // 
            // label_PC_IP
            // 
            this.label_PC_IP.AutoSize = true;
            this.label_PC_IP.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_PC_IP.Location = new System.Drawing.Point(32, 79);
            this.label_PC_IP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_PC_IP.Name = "label_PC_IP";
            this.label_PC_IP.Size = new System.Drawing.Size(60, 20);
            this.label_PC_IP.TabIndex = 29;
            this.label_PC_IP.Text = "PC IP：";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(151, 184);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(137, 26);
            this.textBox2.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(27, 188);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 33;
            this.label1.Text = "Logging Port：";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(151, 132);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(137, 26);
            this.textBox4.TabIndex = 32;
            // 
            // label_MOTION_PORT
            // 
            this.label_MOTION_PORT.AutoSize = true;
            this.label_MOTION_PORT.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_MOTION_PORT.Location = new System.Drawing.Point(30, 133);
            this.label_MOTION_PORT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_MOTION_PORT.Name = "label_MOTION_PORT";
            this.label_MOTION_PORT.Size = new System.Drawing.Size(113, 20);
            this.label_MOTION_PORT.TabIndex = 31;
            this.label_MOTION_PORT.Text = "Motion Port：";
            // 
            // ratio_sim
            // 
            this.ratio_sim.AutoSize = true;
            this.ratio_sim.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ratio_sim.Location = new System.Drawing.Point(19, 239);
            this.ratio_sim.Name = "ratio_sim";
            this.ratio_sim.Size = new System.Drawing.Size(100, 24);
            this.ratio_sim.TabIndex = 35;
            this.ratio_sim.TabStop = true;
            this.ratio_sim.Text = "Simulation";
            this.ratio_sim.UseVisualStyleBackColor = true;
            // 
            // radio_Real
            // 
            this.radio_Real.AutoSize = true;
            this.radio_Real.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radio_Real.Location = new System.Drawing.Point(163, 239);
            this.radio_Real.Name = "radio_Real";
            this.radio_Real.Size = new System.Drawing.Size(97, 24);
            this.radio_Real.TabIndex = 36;
            this.radio_Real.TabStop = true;
            this.radio_Real.Text = "Real robot";
            this.radio_Real.UseVisualStyleBackColor = true;
            // 
            // connect_robot
            // 
            this.connect_robot.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.connect_robot.Location = new System.Drawing.Point(18, 282);
            this.connect_robot.Name = "connect_robot";
            this.connect_robot.Size = new System.Drawing.Size(270, 42);
            this.connect_robot.TabIndex = 37;
            this.connect_robot.Text = "Connect Robot";
            this.connect_robot.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.connect_robot);
            this.groupBox2.Controls.Add(this.radio_Real);
            this.groupBox2.Controls.Add(this.ratio_sim);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.label_MOTION_PORT);
            this.groupBox2.Controls.Add(this.textBox_PC_IP);
            this.groupBox2.Controls.Add(this.label_PC_IP);
            this.groupBox2.Controls.Add(this.textBox_Robot_IP);
            this.groupBox2.Controls.Add(this.label_Robot_IP);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(420, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(304, 343);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connect manager";
            // 
            // radioAbs
            // 
            this.radioAbs.AutoSize = true;
            this.radioAbs.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioAbs.Location = new System.Drawing.Point(198, 26);
            this.radioAbs.Name = "radioAbs";
            this.radioAbs.Size = new System.Drawing.Size(87, 24);
            this.radioAbs.TabIndex = 39;
            this.radioAbs.TabStop = true;
            this.radioAbs.Text = "Absolute";
            this.radioAbs.UseVisualStyleBackColor = true;
            // 
            // radioRel
            // 
            this.radioRel.AutoSize = true;
            this.radioRel.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioRel.Location = new System.Drawing.Point(33, 26);
            this.radioRel.Name = "radioRel";
            this.radioRel.Size = new System.Drawing.Size(80, 24);
            this.radioRel.TabIndex = 38;
            this.radioRel.TabStop = true;
            this.radioRel.Text = "Relative";
            this.radioRel.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(18, 372);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "Axis No:";
            // 
            // comboBox_AxisNo
            // 
            this.comboBox_AxisNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AxisNo.FormattingEnabled = true;
            this.comboBox_AxisNo.Location = new System.Drawing.Point(89, 370);
            this.comboBox_AxisNo.Name = "comboBox_AxisNo";
            this.comboBox_AxisNo.Size = new System.Drawing.Size(99, 25);
            this.comboBox_AxisNo.TabIndex = 41;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(88, 410);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(97, 25);
            this.comboBox1.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(18, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Axis Pos:";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(281, 410);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(97, 25);
            this.comboBox2.TabIndex = 47;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(202, 411);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 46;
            this.label4.Text = "Tool Pos:";
            // 
            // comboBox_Tool_Axis
            // 
            this.comboBox_Tool_Axis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Tool_Axis.FormattingEnabled = true;
            this.comboBox_Tool_Axis.Location = new System.Drawing.Point(281, 369);
            this.comboBox_Tool_Axis.Name = "comboBox_Tool_Axis";
            this.comboBox_Tool_Axis.Size = new System.Drawing.Size(99, 25);
            this.comboBox_Tool_Axis.TabIndex = 45;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(202, 373);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Tool Axis:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(190, 410);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 17);
            this.label7.TabIndex = 48;
            this.label7.Text = "°";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(382, 415);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 17);
            this.label8.TabIndex = 49;
            this.label8.Text = "mm/°";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonSetIO);
            this.groupBox3.Controls.Add(this.buttonMoveZero);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.radioAbs);
            this.groupBox3.Controls.Add(this.radioRel);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(27, 446);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(375, 162);
            this.groupBox3.TabIndex = 50;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Robot Action";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(23, 58);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(126, 34);
            this.button3.TabIndex = 41;
            this.button3.Text = "TCP Move";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(210, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 34);
            this.button2.TabIndex = 40;
            this.button2.Text = "Joint Move";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(438, 566);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(270, 42);
            this.button1.TabIndex = 51;
            this.button1.Text = "Disconnect Robot";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonMoveZero
            // 
            this.buttonMoveZero.Location = new System.Drawing.Point(23, 116);
            this.buttonMoveZero.Name = "buttonMoveZero";
            this.buttonMoveZero.Size = new System.Drawing.Size(126, 34);
            this.buttonMoveZero.TabIndex = 42;
            this.buttonMoveZero.Text = "Move Home";
            this.buttonMoveZero.UseVisualStyleBackColor = true;
            // 
            // buttonSetIO
            // 
            this.buttonSetIO.Location = new System.Drawing.Point(210, 116);
            this.buttonSetIO.Name = "buttonSetIO";
            this.buttonSetIO.Size = new System.Drawing.Size(126, 34);
            this.buttonSetIO.TabIndex = 43;
            this.buttonSetIO.Text = "Set IO";
            this.buttonSetIO.UseVisualStyleBackColor = true;
            // 
            // Robot_Debug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 620);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox_Tool_Axis);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox_AxisNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Robot_Debug";
            this.Text = "ABB Control";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_x;
        private System.Windows.Forms.Label label_x;
        private System.Windows.Forms.Label label_Y;
        private System.Windows.Forms.TextBox textBox_y;
        private System.Windows.Forms.Label label_q0;
        private System.Windows.Forms.TextBox textBox_q0;
        private System.Windows.Forms.Label label_z;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label_q1;
        private System.Windows.Forms.TextBox textBox_q1;
        private System.Windows.Forms.Label label1_q2;
        private System.Windows.Forms.TextBox textBox_q2;
        private System.Windows.Forms.Label label_q3;
        private System.Windows.Forms.TextBox textBox_q3;
        private System.Windows.Forms.Label label_J6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label_J5;
        private System.Windows.Forms.TextBox textBox_J5;
        private System.Windows.Forms.Label label_J4;
        private System.Windows.Forms.TextBox textBox_J4;
        private System.Windows.Forms.Label label_J3;
        private System.Windows.Forms.TextBox textBox_J3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_J2;
        private System.Windows.Forms.Label J1;
        private System.Windows.Forms.TextBox textBox_J1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_Robot_IP;
        private System.Windows.Forms.TextBox textBox_Robot_IP;
        private System.Windows.Forms.TextBox textBox_PC_IP;
        private System.Windows.Forms.Label label_PC_IP;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label_MOTION_PORT;
        private System.Windows.Forms.RadioButton ratio_sim;
        private System.Windows.Forms.RadioButton radio_Real;
        private System.Windows.Forms.Button connect_robot;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioAbs;
        private System.Windows.Forms.RadioButton radioRel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_AxisNo;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_Tool_Axis;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buttonMoveZero;
        private System.Windows.Forms.Button buttonSetIO;
    }
}