﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using CNC_APP.RapidProcess;

namespace CNC_APP
{
    
    public partial class File_Management : Form
    {
        internal string MODFilePath { set; get; }
        ModFileParser _ModParserObj;
        public File_Management()
        {
            InitializeComponent();
            dgvLoad();
        }
        public void dgvLoad()
        {
            this.dataGridView1.Columns[0].HeaderCell.Value = "ID";
            this.dataGridView1.Columns[1].HeaderCell.Value = "MoveType";
            this.dataGridView1.Columns[2].HeaderCell.Value = "Point";
            this.dataGridView1.Columns[3].HeaderCell.Value = "External";
            this.dataGridView1.Columns[4].HeaderCell.Value = "Conf";
            this.dataGridView1.Columns[5].HeaderCell.Value = "Speed";
            this.dataGridView1.Columns[6].HeaderCell.Value = "Zone";
            this.dataGridView1.Columns[7].HeaderCell.Value = "Tool";
            this.dataGridView1.Columns[8].HeaderCell.Value = "Wobj";
            
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
           
          //  MessageBox.Show(MODFilePath);
        }

        private void btn_AddFile_Click(object sender, EventArgs e)
        {

            btn_AddFile.Enabled = false;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件夹";
            dialog.Filter = "所有文件(*.*)|*.*";
            string FilePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MODFilePath = dialog.FileName;
                _ModParserObj = new ModFileParser();
                int LineNum = _ModParserObj.LoadFile(MODFilePath);
                string sLineNum = LineNum.ToString();
                string text = "Rapid line number = " + sLineNum;
                if (MessageBox.Show(text, "Rapid Info", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    string filename = Path.GetFileName(MODFilePath);
                    treeView_DirList.Nodes.Add(filename);
                }
                else
                {
                    MessageBox.Show("Error","Please check");
                }
            }
            //防止多次点击按钮
            Application.DoEvents();
            btn_AddFile.Enabled = true;
        }

        private void btn_Parsing_Click(object sender, EventArgs e)
        {
            //1.open the rapid mod file
          //  ModFileParser ModParserObj = new ModFileParser();
            _ModParserObj.ModuleParser();
            _ModParserObj.JsonWriter("hello.json");
            //2. generate the json file
            MessageBox.Show("Parser done", "Succeed");

            //显示点数：第一个点和第二个点；
        }

        private void btn_DelFile_Click(object sender, EventArgs e)
        {
            //delete file
        }

        private void btn_GenSeqFile_Click(object sender, EventArgs e)
        {
            List<RapidJsonDeserialization.Move> moveCmdList = new List<RapidJsonDeserialization.Move>();
            _ModParserObj.JsonParser("hello.json", ref moveCmdList);
     
            int nSize = moveCmdList.Count();
            int nStart = 0;
            //显示坐标数量
            textBox_EndPoint.Text = nSize.ToString();
            textBox_StartPoint.Text = nStart.ToString();
            //显示坐标
            string[] sID = new string[nSize];
            string[] sMoveType = new string[nSize];
            string[] sPoint = new string[nSize];
            string[] sExternal_Axis = new string[nSize];
            string[] sConfiguration = new string[nSize];
            string[] sSpeed = new string[nSize];
            string[] sZone = new string[nSize];
            string[] sTool = new string[nSize];
            string[] sWobj = new string[nSize];

            for (int nIdx = 0; nIdx < nSize; nIdx++)
            {
                sID[nIdx] = moveCmdList[nIdx].Id.ToString();

                if (moveCmdList[nIdx].MoveType == RapidJsonDeserialization.MOVECMD_TYPE.MOVEABSJ)
                {
                    sMoveType[nIdx] = "MoveAbsJ";
                }
                else if (moveCmdList[nIdx].MoveType == RapidJsonDeserialization.MOVECMD_TYPE.MOVEJ)
                {
                    sMoveType[nIdx] = "MoveJ";
                }
                else if (moveCmdList[nIdx].MoveType == RapidJsonDeserialization.MOVECMD_TYPE.MOVEL)
                {
                    sMoveType[nIdx] = "MoveL";
                }
                else if (moveCmdList[nIdx].MoveType == RapidJsonDeserialization.MOVECMD_TYPE.MOVEC)
                {
                    sMoveType[nIdx] = "MoveC";
                }
                sPoint[nIdx] = moveCmdList[nIdx].Point;
                sExternal_Axis[nIdx] = moveCmdList[nIdx].External_Axis;
                sConfiguration[nIdx] = moveCmdList[nIdx].Configuration;
                sSpeed[nIdx] = moveCmdList[nIdx].Speed;
                sZone[nIdx] = moveCmdList[nIdx].Zone;
                sTool[nIdx] = moveCmdList[nIdx].Tool;
                sWobj[nIdx] = moveCmdList[nIdx].Wobj;
                this.dataGridView1.Rows.Add(sID[nIdx], sMoveType[nIdx], sPoint[nIdx], sExternal_Axis[nIdx],
                                            sConfiguration[nIdx], sSpeed[nIdx], sZone[nIdx], sTool[nIdx], sWobj[nIdx]);
            }
           
         
                   
        }
    }
}
