﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNC_APP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ABB_CONNECT_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
            pictureBox_LOGO.BackgroundImage = Properties.Resources.title;
        }

        private void EXIT_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Timer.Text = System.DateTime.Now.ToString("yyyy年MM月dd日 HH:mm:ss");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // machine procedure debug;
            machine_proc machine_ProcObj = new machine_proc();
            machine_ProcObj.Show();
        }

        private void YKSRC_Click(object sender, EventArgs e)
        {
            CNC Obj = new CNC();
            Obj.Show();
        }
        // open robot control Form
        private void Debug_Click(object sender, EventArgs e)
        {
            Robot_Debug Robot_DebugObj = new Robot_Debug();
            Robot_DebugObj.Show();
        }

        private void File_Manager_Click(object sender, EventArgs e)
        {
            File_Management Obj = new File_Management();
            Obj.Show();
        }
    }
}
