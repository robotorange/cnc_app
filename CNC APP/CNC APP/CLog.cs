﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Import log4net classes.
using log4net;
using log4net.Config;

//[assembly: log4net.Config.XmlConfigurator(ConfigFile = "..\\ES4SA_CORE\\App.config", Watch = true)]
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Logger
{
    /// <summary>
    /// Defined level：FATAL、ERROR、WARN、INFO、DEBUG 
    /// </summary>
    public class CLog
    {
        private static readonly ILog Log = LogManager.GetLogger("RollingLogFileAppender");
        // private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region DEBUG  
        public static void Debug(string write)
        {
            Log.Debug("Logging:" + write);
        }
        public static void Debug(string write, Exception ex)
        {
            Log.Debug("Logging:" + write + ". Error Record:" + ex.ToString());
        }
        #endregion

        #region INFO 
        public static void Info(string write)
        {
            Log.Info("Logging:" + write);
        }
        public static void Info(string write, Exception ex)
        {
            Log.Info("Logging:" + write + ". Error Record:" + ex.ToString());
        }
        #endregion

        #region WARN 
        public static void Warn(string write)
        {
            Log.Warn("Logging:" + write);
        }
        public static void Warn(string write, Exception ex)
        {
            Log.Warn("Logging:" + write + ". Error Record:" + ex.ToString());
        }
        #endregion

        #region ERROR
        public static void Error(string write)
        {
            Log.Error("Logging:" + write);
        }
        public static void Error(string write, Exception ex)
        {
            Log.Error("Logging:" + write + ". Error Record:" + ex.ToString());
        }
        #endregion

        #region FATAL 
        public static void Fatal(string write)
        {
            Log.Fatal("Logging:" + write);
        }
        public static void Fatal(string write, Exception ex)
        {
            Log.Fatal("Logging:" + write + ". Error Record:" + ex.ToString());
        }
        #endregion


    }
}
