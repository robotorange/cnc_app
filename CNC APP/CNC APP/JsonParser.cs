﻿/**
* @file      JsonParser.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Mar 31, 2020
* @brief     base class for json parser function  ; 
*/
using System;
using System.Text.Json;


namespace JsonParser
{
    /// <summary>
    /// Parse the json file
    /// </summary>
    public class CJsonParser
    {
        /// <summary>
        /// Get the result of T type object from parse the JSON file.
        /// </summary>
        /// <typeparam name="T">The object type corresponding to JSON file.</typeparam>
        /// <param name="filePath">The path of JSON file</param>
        /// <returns>The result of T type object from parse the JSON file</returns>
        public static T GetJsonObject<T>(string filePath) where T : class, new()
        {
            try
            {
                var options = new JsonSerializerOptions
                {
                    WriteIndented = true
                };
                string jsonContext = System.IO.File.ReadAllText(filePath);
                var jsonObject = JsonSerializer.Deserialize<T>(jsonContext, options);

                return jsonObject;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static void WriteJsonFile<T>(T jsonValue, string jsonFilePath) where T : class, new()
        {
            try
            {
                var options = new JsonSerializerOptions
                {
                    WriteIndented = true
                };

                string jsonContext = JsonSerializer.Serialize<T>(jsonValue, options);
                System.IO.File.WriteAllText(jsonFilePath, jsonContext);

            }
            catch (Exception)
            {
                return;
            }

        }
    }
}
