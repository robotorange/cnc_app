﻿/**
* @file      RapidJsonDeSerialization.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Apr 14, 2020
* @brief     Get the internal Json file and deserialzate the data structure(string) into number  ; 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonParser;

namespace CNC_APP.RapidProcess
{
    class RapidJsonDeserialization
    {
        public List<Move> _moveCmdList{ set; get; }
        class ABBPos
        {
            public class Pose6D
            {
                public double q1 { get; set; } = 0.0;
                public double q2 { get; set; } = 0.0;
                public double q3 { get; set; } = 0.0;
                public double q4 { get; set; } = 0.0;
                public double q5 { get; set; } = 0.0;
                public double q6 { get; set; } = 0.0;
            }

            public class Pose
            {
                public double x { get; set; } = 0.0;
                public double y { get; set; } = 0.0;
                public double z { get; set; } = 0.0;
                public double q0 { get; set; } = 0.0;
                public double qx { get; set; } = 0.0;
                public double qy { get; set; } = 0.0;
                public double qz { get; set; } = 0.0;
            }

            public class Pose3D
            {
                public double x { get; set; } = 0.0;
                public double y { get; set; } = 0.0;
                public double z { get; set; } = 0.0;
            }
            public class Ori
            {
                public double q0 { get; set; } = 0.0;
                public double qx { get; set; } = 0.0;
                public double qy { get; set; } = 0.0;
                public double qz { get; set; } = 0.0;
            }
            public class Speed
            {
                public double tcp { get; set; } = 0.0;
                public double ori { get; set; } = 0.0;
            }
            public class Zone
            {
                public double fine { get; set; } = 0.0;
                public double tcp_mm { get; set; } = 0.0;
                public double ori_mm { get; set; } = 0.0;
                public double ori_deg { get; set; } = 0.0;
            }
        }
        public enum MOVECMD_TYPE
        {
            MOVEABSJ = 1,           
            MOVEJ = 2,
            MOVEL = 3,
            MOVEC = 4
        }
 
        public class wobjdata
        {
            public bool RobHold { get; set; } = false;
            public bool Ufprog { get; set; } = true;
            public string Ufmec { get; set; } = "";
            public string Uframe { get; set; } = ""; //pose type
            public string Oframe { get; set; } = "";//pose type

        }
        public class tooldata
        {
            public bool RobHold { get; set; } = true;
            public string Tframe { get; set; } = "";//pose type
            public string Tload { get; set; } = "";
        }
        public abstract class Move
        {
            public abstract int Id { get; set; }
            public abstract MOVECMD_TYPE MoveType { get; set; }
            public abstract string Point { get; set; }
            public abstract string External_Axis { get; set; }
            public abstract string Configuration { get; set; }
            public abstract string Speed { get; set; }
            public abstract string Zone { get; set; }
            public abstract string Tool { get; set; }
            public abstract string Wobj { get; set; }
        }
        public class MoveAbsJ : Move
        {
            public override int Id { get; set; } = 0;
            public override MOVECMD_TYPE MoveType { get; set; } = MOVECMD_TYPE.MOVEABSJ;
            public override string Point { get; set; } = "0.0,0.0,0.0,0.0,0.0,0.0";
            public override string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
            public override string Configuration { get; set; } = "";
            public override string Speed { get; set; } = "v50";
            public override string Zone { get; set; } = "fine";
            public override string Tool { get; set; } = "tool2";
            public override string Wobj { get; set; } = "";
        }

        public class MoveL : Move
        {
            public override int Id { get; set; } = 1;
            public override MOVECMD_TYPE MoveType { get; set; } = MOVECMD_TYPE.MOVEJ;
            public override string Point { get; set; } = "0.0,0.0,0.0,0,1,0,0";
            public override string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
            public override string Configuration { get; set; } = "-1,0,-1,1";
            public override string Speed { get; set; } = "50.00,500,5000,1000";
            public override string Zone { get; set; } = "fine";
            public override string Tool { get; set; } = "tool2";
            public override string Wobj { get; set; } = "wobj2";

        }

        public class MoveJ : Move
        {
            public override int Id { get; set; } = 2;
            public override MOVECMD_TYPE MoveType { get; set; } = MOVECMD_TYPE.MOVEJ;
            public override string Point { get; set; } = "0.0,0.0,0.0,0,1,0,0";
            public override string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
            public override string Configuration { get; set; } = "-1,0,-1,1";
            public override string Speed { get; set; } = "50.00,500,5000,1000";
            public override string Zone { get; set; } = "fine";
            public override string Tool { get; set; } = "tool2";
            public override string Wobj { get; set; } = "wobj2";

        }
        public class MoveC : Move
        {
            public override int Id { get; set; } = 3;
            public override MOVECMD_TYPE MoveType { get; set; } = MOVECMD_TYPE.MOVEC;
            public override string Point { get; set; } = "1.0,0.0,0.0,0,1,0,0,2.0,0.0,0.0,0,1,0,0";
            public override string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9,9E9,9E9,9E9,9E9,9E9,9E9";
            public override string Configuration { get; set; } = "0,0,0,1,0,0,0,1";
            public override string Speed { get; set; } = "50.00,500,5000,1000";
            public override string Zone { get; set; } = "fine";
            public override string Tool { get; set; } = "tool2";
            public override string Wobj { get; set; } = "wobj2";
        }


        public void JsonFileParser(string Path, ref RapidJsonSerialization.ActionJson val)
        {
            val = CJsonParser.GetJsonObject<RapidJsonSerialization.ActionJson>(Path);
            //RapidJsonSerialization.MoveCmd 
            int numbers = val.moveCmds.Count();
            _moveCmdList = new List<Move>();
            // 区分moveAbsj,moveJ,moveL,moveC压缩到向量中
            for (int nIdx =0;nIdx<numbers;nIdx++)
            {
                if (val.moveCmds[nIdx].MoveType == MOVE_TYPE.MOVEABSJ)
                {
                    MoveAbsJ MoveAbsJ_Obj = new MoveAbsJ();
                    MoveAbsJ_Obj.Id = val.moveCmds[nIdx].Id;
                    MoveAbsJ_Obj.MoveType = MOVECMD_TYPE.MOVEABSJ;
                    MoveAbsJ_Obj.Point = val.moveCmds[nIdx].Point;
                    MoveAbsJ_Obj.External_Axis = val.moveCmds[nIdx].External_Axis;
                    MoveAbsJ_Obj.Configuration = val.moveCmds[nIdx].Configuration;
                    MoveAbsJ_Obj.Speed = val.moveCmds[nIdx].Speed;
                    MoveAbsJ_Obj.Zone = val.moveCmds[nIdx].Zone;
                    MoveAbsJ_Obj.Tool = val.moveCmds[nIdx].Tool;
                    MoveAbsJ_Obj.Wobj = val.moveCmds[nIdx].Wobj;
                    _moveCmdList.Add(MoveAbsJ_Obj);
                }
                else if (val.moveCmds[nIdx].MoveType == MOVE_TYPE.MOVEL)
                {
                    MoveL MovL_Obj = new MoveL();
                    MovL_Obj.Id = val.moveCmds[nIdx].Id;
                    MovL_Obj.MoveType = MOVECMD_TYPE.MOVEL;
                    MovL_Obj.Point = val.moveCmds[nIdx].Point;
                    MovL_Obj.External_Axis = val.moveCmds[nIdx].External_Axis;
                    MovL_Obj.Configuration = val.moveCmds[nIdx].Configuration;
                    MovL_Obj.Speed = val.moveCmds[nIdx].Speed;
                    MovL_Obj.Zone = val.moveCmds[nIdx].Zone;
                    MovL_Obj.Tool = val.moveCmds[nIdx].Tool;
                    MovL_Obj.Wobj = val.moveCmds[nIdx].Wobj;
                    _moveCmdList.Add(MovL_Obj);
                }
                else if (val.moveCmds[nIdx].MoveType == MOVE_TYPE.MOVEJ)
                {
                    MoveJ MovJ_Obj = new MoveJ();
                    MovJ_Obj.Id = val.moveCmds[nIdx].Id;
                    MovJ_Obj.MoveType = MOVECMD_TYPE.MOVEJ;
                    MovJ_Obj.Point = val.moveCmds[nIdx].Point;
                    MovJ_Obj.External_Axis = val.moveCmds[nIdx].External_Axis;
                    MovJ_Obj.Configuration = val.moveCmds[nIdx].Configuration;
                    MovJ_Obj.Speed = val.moveCmds[nIdx].Speed;
                    MovJ_Obj.Zone = val.moveCmds[nIdx].Zone;
                    MovJ_Obj.Tool = val.moveCmds[nIdx].Tool;
                    MovJ_Obj.Wobj = val.moveCmds[nIdx].Wobj;
                    _moveCmdList.Add(MovJ_Obj);
                }
                else if (val.moveCmds[nIdx].MoveType == MOVE_TYPE.MOVEC)
                {
                    MoveC MovC_Obj = new MoveC();
                    MovC_Obj.Id = val.moveCmds[nIdx].Id;
                    MovC_Obj.MoveType = MOVECMD_TYPE.MOVEC;
                    MovC_Obj.Point = val.moveCmds[nIdx].Point;
                    MovC_Obj.External_Axis = val.moveCmds[nIdx].External_Axis;
                    MovC_Obj.Configuration = val.moveCmds[nIdx].Configuration;
                    MovC_Obj.Speed = val.moveCmds[nIdx].Speed;
                    MovC_Obj.Zone = val.moveCmds[nIdx].Zone;
                    MovC_Obj.Tool = val.moveCmds[nIdx].Tool;
                    MovC_Obj.Wobj = val.moveCmds[nIdx].Wobj;
                    _moveCmdList.Add(MovC_Obj);
                }
                else
                {
                    continue;
                }
            }
            int nsize = _moveCmdList.Count();
        }

    }
}


//      public interface IMove
//      {
//          int ID { get; set; }
//          bool IfVal { get; set; }
//          bool IfContinue { get; set; }
//          int MoveNumber { get; set; }
////        MOVE MoveType { get; set; }
//      }
//      public class MoveAbsJ : IMove
//      {
//          public int ID { get; set; } = 0;
//          public bool IfVal { get; set; } = false;
//          public bool IfContinue { get; set; } = false;
//          public int MoveNumber { get; set; } = 1;
////          public MOVE MoveType { get; set; } = MOVE.MoveAbsJ;
//          // Special commands for MoveAbsJ
//          public string Point6d { get; set; } = "0.0,0.0,0.0,0.0,0.0,0.0";
//          public string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
//          public string Speed { get; set; } = "v50";
//          public string Zone { get; set; } = "fine";
//          public string Tool { get; set; } = "tool2";
//      }

//      public class MoveL : IMove
//      {
//          public int ID { get; set; } = 0;
//          public bool IfVal { get; set; } = false;
//          public bool IfContinue { get; set; } = false;
//          public int MoveNumber { get; set; } = 1;
////          public MOVE MoveType { get; set; } = MOVE.MoveL;
//          // Special commands for MoveL
//          public string Pos3d { get; set; } = "0.0,0.0,0.0";
//          public string Ori { get; set; } = "0,1,0,0";
//          public string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
//          public string Configuration { get; set; } = "-1,0,-1,1";
//          public string Speed { get; set; } = "50.00,500,5000,1000";
//          public string Zone { get; set; } = "fine";
//          public string Tool { get; set; } = "tool2";
//          public string Wobj { get; set; } = "wobj2";
//      }

//      public class MoveJ : IMove
//      {
//          public int ID { get; set; } = 0;
//          public bool IfVal { get; set; } = false;
//          public bool IfContinue { get; set; } = false;
//          public int MoveNumber { get; set; } = 1;
//  //        public MOVE MoveType { get; set; } = MOVE.MoveJ;
//          // Special commands for MoveJ
//          public string Point3d { get; set; } = "0.0,0.0,0.0";
//          public string Ori { get; set; } = "0,1,0,0";
//          public string External_Axis { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
//          public string Configuration { get; set; } = "-1,0,-1,1";
//          public string Speed { get; set; } = "50.00,500,5000,1000";
//          public string Zone { get; set; } = "fine";
//          public string Tool { get; set; } = "tool2";
//          public string Wobj { get; set; } = "wobj2";
//      }

//      public class MoveC : IMove
//      {
//          public int ID { get; set; } = 0;
//          public bool IfVal { get; set; } = false;
//          public bool IfContinue { get; set; } = false;
//          public int MoveNumber { get; set; } = 1;
// //         public MOVE MoveType { get; set; } = MOVE.MoveL;
//          // Special commands for MoveC
//          public string Point3d_1 { get; set; } = "0.0,0.0,0.0";
//          public string Ori_1 { get; set; } = "0,1,0,0";
//          public string External_1 { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
//          public string Conf_1 { get; set; } = "0,0,0,1";
//          public string Point3d_2 { get; set; } = "0.0,0.0,0.0";
//          public string Ori_2 { get; set; } = "0,1,0,0";
//          public string External_2 { get; set; } = "9E9,9E9,9E9,9E9,9E9,9E9";
//          public string Conf_2 { get; set; } = "0,0,0,1";
//          public string Speed { get; set; } = "50.00,500,5000,1000";
//          public string Zone { get; set; } = "fine";
//          public string Tool { get; set; } = "tool2";
//          public string Wobj { get; set; } = "wobj2";
//      }