﻿/**
* @file      ModParser.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Apr 6, 2020
* @brief     1.Read and parsing the ABB robot rapid file;
*            2.the main process 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Logger;
using System.Text.RegularExpressions;


namespace CNC_APP.RapidProcess
{
    
    class ModFileParser
    {
        internal List<string> _LineCode { set; get; }
        internal string _RapidFileName { set; get; }
        public string _JsonPath { set; get; }
        internal int _MoveID { set; get; } = 0;

        RapidJsonSerialization.RapidJsonWriter _jsonWriterObj = new RapidJsonSerialization.RapidJsonWriter();
        public RapidJsonDeserialization _jsonParserObj = new RapidJsonDeserialization();
        RapidJsonSerialization.ActionJson _actionJsonObj = new RapidJsonSerialization.ActionJson();
        CommonFunction _commFuncObj = new CommonFunction();

        public int LoadFile(string sFileName)
        {
            //string sModulePath = System.AppDomain.CurrentDomain.BaseDirectory;
            //sModulePath = sModulePath.Replace("bin\\", "");
            //sModulePath = sModulePath.Replace("Debug\\", "");
            //sModulePath += @"MOD\\";

            //// string sFileName = @"TestA.MOD";
            //sFileName = sModulePath + sFileName;
            string sPath = sFileName;
            _RapidFileName = Path.GetFileName(sPath);
            int lineNum = 0;
            try
            {
                // 创建一个 StreamReader 的实例来读取文件 
                // using 语句也能关闭 StreamReader

                using (StreamReader FileReader = File.OpenText(sPath))
                {
                    string line;
                    this._LineCode = new List<string>();
                    // 从文件读取并显示行，直到文件的末尾 
                    while ((line = FileReader.ReadLine()) != null)
                    {
                        //Console.WriteLine(line);
                        _LineCode.Add(line);
                        lineNum++;
                    }
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                // 向用户显示出错消息
                CLog.Error("The file could not be read:");
                CLog.Error(e.Message);
            }
            return lineNum;

        }
        public void ModuleParser() //Binary search
        {
            int nLineNum = _LineCode.Count(); // get Mod Line number
          
            int i = 0;
            int end = nLineNum;
            int atMODULE = 0;
            int atPROC = 0;
            int atTool = 0;
            int atWobj = 0;
            int atComment = 0;
            string ModuleName = "";
            string WobjInfo = "";
            string ToolInfo = "";
            string ProcInfo = "";
         
            while (i < end)
            {
                // add Module
                if (((atMODULE = _LineCode[i].IndexOf("MODULE")) != -1) && (ModuleName == "") && ((atComment = _LineCode[i].IndexOf("!")) == -1))
                {
                    char[] chs = { ' ' };
                    string[] res = _LineCode[i].Split(chs, options: StringSplitOptions.RemoveEmptyEntries);
                    ModuleName = res[1];

                }
                // add  wobJdata
                else if (((atWobj = _LineCode[i].IndexOf("PERS wobjdata")) != -1) && ((atComment = _LineCode[i].IndexOf("!")) == -1))
                {
                    double[] Point3d;
                    double[] Orientation;
                    WobjCmdParser WobjParserObj = new WobjCmdParser();
                    WobjParserObj.Wobjdata(_LineCode[i], out Point3d, out Orientation);
                    _commFuncObj.Pos3dAndOri2Pos(Point3d, Orientation, out WobjInfo);
                    i += 1;

                }
                // add tooldata 
                else if (((atTool = _LineCode[i].IndexOf("PERS tooldata")) != -1) && ((atComment = _LineCode[i].IndexOf("!")) == -1))
                {
                    double[] Point3d;
                    double[] Orientation;
                    ToolCmdParser ToolParserObj = new ToolCmdParser();
                    ToolParserObj.Tooldata(_LineCode[i], out Point3d, out Orientation);
                    _commFuncObj.Pos3dAndOri2Pos(Point3d, Orientation, out ToolInfo);
                    i += 1;
                }

                // add PROC
                else if (((atPROC = _LineCode[i].IndexOf("PROC")) != -1) && ((atComment = _LineCode[i].IndexOf("!")) == -1))
                {
                    char[] chs = { ' ' };
                    string[] res = _LineCode[i].Split(chs, options: StringSplitOptions.RemoveEmptyEntries);
                    ProcInfo = res[1];
                    ProcInfo = ProcInfo.Replace("()", string.Empty);
                    //Console.WriteLine("{0} =  {1}", i, ProcName);                
                    // generate the json file and set tool and Wobj info
                    _jsonWriterObj.InitRapidJson( _RapidFileName, ModuleName,ProcInfo, out _actionJsonObj);
                    _jsonWriterObj.WobjdataWriter(ref _actionJsonObj, WobjInfo);
                    _jsonWriterObj.TooldataWriter(ref _actionJsonObj, ToolInfo);
                    ProcParser(i);
                    break;
                }               
                else
                {
                    i += 1;
                }
            }
            
        }

        public void ProcParser(int PROCStLine)
        {
            int startLine = PROCStLine;
            List<string> LineCopy = _LineCode.Skip(startLine).ToList<string>();
            foreach (var line in LineCopy)
            {
                int at = 0;
                startLine += 1;
                if ((at = line.IndexOf("Move")) != -1)
                {
                    MoveParser(line);
                    _MoveID = _MoveID + 1;

                }
            }
        }
        public void JsonWriter(string Path)
        {
            _jsonWriterObj.JsonFileWriter(Path, _actionJsonObj);
        }

        public void JsonParser(string Path,ref  List<RapidJsonDeserialization.Move> moveCmdList)
        {
            RapidJsonSerialization.ActionJson obj = new RapidJsonSerialization.ActionJson();
            _jsonParserObj.JsonFileParser(Path, ref obj);
            moveCmdList =  _jsonParserObj._moveCmdList;
        }

        public void MoveParser(string MoveString )
        {
            int atMoveAbJ = 0;
            int atMoveJ = 0;
            int atMoveL = 0;
            int atMoveC = 0;
            int atComment = 0;
            MoveCmdParser MoveCmdParserObj = new MoveCmdParser();

            //1.Separate MoveString into MoveAbsJ，MoveJ，MoveL，MoveC
            if (((atMoveAbJ = MoveString.IndexOf("MoveAbsJ")) != -1) && ((atComment = MoveString.IndexOf("!")) == -1))
            {
                double[] Joints;
                string sJoints;
                string[] External;
                string sExternal;
                string sSpd;
                string sZone;
                string sTool;
                MoveCmdParserObj.MoveAbsJParser(MoveString, out Joints, out External, out sSpd, out sZone, out sTool);
                _commFuncObj.Double2Str(Joints,6,out sJoints);
                _commFuncObj.Substr2Str(External, out sExternal);
                _jsonWriterObj.MoveAbsJWriter(ref _actionJsonObj, _MoveID, sJoints, sExternal, sSpd, sZone, sTool,true);
               
            }
            else if ((atMoveJ = MoveString.IndexOf("MoveJ")) != -1)
            {
                double[] Point3d ;
                string sPoint3d;
                double[] Orientation;
                string sOrientation;
                int[] conf;
                string sConf;
                string[] External ;
                string sExternal;
                double[] Spd4 ;
                string sSpd;
                string sZone ;
                string sTool ;
                string sWobj ;
                MoveCmdParserObj.MoveJParser(MoveString, out Point3d, out Orientation, out conf,
                            out External, out Spd4, out sZone, out sTool, out sWobj);
                _commFuncObj.Double2Str(Point3d, 3, out sPoint3d);
                _commFuncObj.Double2Str(Orientation,4, out sOrientation);
                _commFuncObj.Int2Str(conf, 4, out sConf);
                _commFuncObj.Substr2Str(External, out sExternal);
                _commFuncObj.Double2Str(Spd4, 4, out sSpd);
                _jsonWriterObj.MoveJWriter(ref _actionJsonObj, _MoveID, sPoint3d, sOrientation, sConf, sExternal, sSpd, sZone, sTool, sWobj, true);

            }
            else if ((atMoveL = MoveString.IndexOf("MoveL")) != -1)
            {
                double[] Point3d;
                string sPoint3d;
                double[] Orientation;
                string sOrientation;
                int[] conf;
                string sConf;
                string[] External;
                string sExternal;
                double[] Spd4;
                string sSpd;
                string sZone;
                string sTool;
                string sWobj;
                MoveCmdParserObj.MoveLParser(MoveString, out Point3d, out Orientation, out conf,
                                            out External, out Spd4, out sZone, out sTool, out sWobj);
                _commFuncObj.Double2Str(Point3d, 3, out sPoint3d);
                _commFuncObj.Double2Str(Orientation, 4, out sOrientation);
                _commFuncObj.Int2Str(conf, 4, out sConf);
                _commFuncObj.Substr2Str(External, out sExternal);
                _commFuncObj.Double2Str(Spd4, 4, out sSpd);
                _jsonWriterObj.MoveLWriter(ref _actionJsonObj, _MoveID, sPoint3d, sOrientation, sConf, sExternal, sSpd, sZone, sTool, sWobj, true);
            }
            else if ((atMoveC = MoveString.IndexOf("MoveC")) != -1)
            {
                double[] Point3d_1 = { 0.0};
                string sPoint3d_1;
                double[] Orientation_1 = { 0.0 };
                string sOrientation_1;
                int[] conf_1 = { 0 };
                string sConf_1;
                string[] External_1= { ""};
                string sExternal_1;
                double[] Point3d_2 = { 0.0 };
                string sPoint3d_2;
                double[] Orientation_2 = { 0.0 };
                string sOrientation_2;

                int[] conf_2 = { 0 };
                string sConf_2;
                string[] External_2 = { "" };
                string sExternal_2;
                double[] Spd4 = { 0.0 };
                string sSpd;
                string sZone;
                string sTool;
                string sWobj;

                MoveCmdParserObj.MoveCParser(MoveString, out Point3d_1, out Orientation_1, out conf_1, out External_1,
                            out Point3d_2, out Orientation_2, out conf_2, out External_2,
                            out Spd4, out sZone, out sTool, out sWobj);

                _commFuncObj.Double2Str(Point3d_1, 3, out sPoint3d_1);
                _commFuncObj.Double2Str(Orientation_1, 4, out sOrientation_1);
                _commFuncObj.Int2Str(conf_1, 4, out sConf_1);
                _commFuncObj.Substr2Str(External_1, out sExternal_1);
                _commFuncObj.Double2Str(Point3d_2, 3, out sPoint3d_2);
                _commFuncObj.Double2Str(Orientation_2, 4, out sOrientation_2);
                _commFuncObj.Int2Str(conf_2, 4, out sConf_2);
                _commFuncObj.Substr2Str(External_2, out sExternal_2);
                _commFuncObj.Double2Str(Spd4, 4, out sSpd);

                _jsonWriterObj.MoveCWriter(ref _actionJsonObj, _MoveID, sPoint3d_1, sOrientation_1, sConf_1, sExternal_1, 
                                            sPoint3d_2, sOrientation_2, sConf_2, sExternal_2, sSpd, sZone, sTool, sWobj, true);
            }
            else
            {
                CLog.Info("no move cmd");
                return;
            }

        }
            
            


        
    }
}