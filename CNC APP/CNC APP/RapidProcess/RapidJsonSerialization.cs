﻿/**
* @file      RapidJsonSerialization.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Apr 12, 2020
* @brief     transfer rapid command into Json and Serialzation ; 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonParser;

namespace CNC_APP.RapidProcess
{
    public enum MOVE_TYPE
    {
        MOVEABSJ = 1,
        MOVEJ = 2,
        MOVEL = 3,
        MOVEC = 4
    }

    class RapidJsonSerialization
    {
        public class MoveCmd
        {
            public int Id { get; set; }
            public MOVE_TYPE MoveType { get; set; }
            public string Point { get; set; }
            public string External_Axis { get; set; }
            public string Configuration { get; set; }
            public string Speed { get; set; }
            public string Zone { get; set; }
            public string Tool { get; set; }
            public string Wobj { get; set; }
        }

        public class Wobjdata
        {
            public bool RobHold { get; set; } = false;
            public bool Ufprog { get; set; } = true;
            public string Ufmec { get; set; } = ""; //user frame mechanical unit
            public string Uframe { get; set; } = ""; //pose type
            public string Oframe { get; set; } = "";//pose type
        }

        public class Tooldata
        {
            public bool RobHold { get; set; } = true;
            public string Tframe { get; set; } = "";//pose type
            public string Tload { get; set; } = "";
        }

 
        [Serializable]
        public class ActionJson
        {
            public string RapidName { get; set; } 
            public string Timestamp { get; set; } // time stamp
            public string Module { get; set; }
            public string PROC { get; set; }
            public Wobjdata wobjData_Obj { get; set; } 
            public Tooldata tooldata_Obj { get; set; } 
            //Move command
            public List<MoveCmd> moveCmds { get; set; }

        }

        public class RapidJsonWriter
        {
            string GetTimeStamp()
            {
                TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                string timeNow = Convert.ToInt64(ts.TotalSeconds).ToString();
                string time = timeNow.Substring(0, 10);
                double timestamp = Convert.ToInt64(time);
                System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
                dateTime = dateTime.AddSeconds(timestamp).ToLocalTime();
                string Now = dateTime.ToString();
                return Now;
            }

            public void InitRapidJson(string Rapid,string Module,string PROC, out ActionJson Obj)
            {
                //初始化
                Obj = new ActionJson();
                Obj.RapidName = Rapid;
                Obj.Timestamp = GetTimeStamp();
                Obj.Module = Module;
                Obj.PROC = PROC;
                Obj.wobjData_Obj = new Wobjdata();
                Obj.tooldata_Obj = new Tooldata();
                Obj.moveCmds = new List<MoveCmd>();

            }

            public void JsonFileWriter(string Path,ActionJson Obj)
            {
                CJsonParser.WriteJsonFile<ActionJson>(Obj, Path);
            }



            public void WobjdataWriter(ref ActionJson Obj, string Point3D, string Ori)
            {
                string Uframe = Point3D + "," + Ori;
                Obj.wobjData_Obj.Uframe = Uframe;

            }

            public void WobjdataWriter(ref ActionJson Obj, string WobjInfo)
            {
                Obj.wobjData_Obj.Uframe = WobjInfo;

            }
            public void TooldataWriter(ref ActionJson Obj, string Point3D, string Ori)
            {
                string Tframe = Point3D + "," + Ori;
                Obj.tooldata_Obj.Tframe = Tframe;

            }

            public void TooldataWriter(ref ActionJson Obj, string ToolInfo)
            {            
                Obj.tooldata_Obj.Tframe = ToolInfo;
            }

            public void MoveAbsJWriter(ref ActionJson Obj, int ID, string Point6D, string External, string Spd, string Zone, string tool, bool IfVal)
            {
                MoveCmd MoveAbsJ_Obj = new MoveCmd
                {
                    Id = ID,
                    MoveType = MOVE_TYPE.MOVEABSJ,               
                    Point = Point6D,
                    External_Axis = External,
                    Speed = Spd,
                    Zone = Zone,
                    Tool = tool
                };
                Obj.moveCmds.Add(MoveAbsJ_Obj);
            }

            public void MoveLWriter(ref ActionJson Obj, int ID, string Point3D, string Ori, string conf, string External, string Spd, string Zone, string tool, string Wobj, bool IfVal)
            {
                string Point_L = Point3D + "," + Ori;
                MoveCmd MoveL_Obj = new MoveCmd
                {
                    Id = ID,
                    //IfContinueMove = false,
                    //NumOfMoveL = 1,      
                    MoveType = MOVE_TYPE.MOVEL,
                    Point = Point_L,
                    Configuration = conf,
                    External_Axis = External,
                    Speed = Spd,
                    Zone = Zone,
                    Tool = tool,
                    Wobj = Wobj,
                };
                Obj.moveCmds.Add(MoveL_Obj);
            }

            public void MoveJWriter(ref ActionJson Obj, int ID, string Pos3D, string Ori, string conf, string External, string Spd, string Zone, string tool, string Wobj, bool IfVal)
            {
                string Point_J = Pos3D + "," + Ori;
                MoveCmd MoveJ_Obj = new MoveCmd
                {
                    Id = ID,
                    MoveType = MOVE_TYPE.MOVEJ,
                    Point = Point_J,
                    Configuration = conf,
                    External_Axis = External,
                    Speed = Spd,
                    Zone = Zone,
                    Tool = tool,
                    Wobj = Wobj,
                };
                Obj.moveCmds.Add(MoveJ_Obj);
            }

            public void MoveCWriter(ref ActionJson Obj, int ID, string Pt3D1, string Ori1, string conf1, string External_1,
                                    string Pt3D2, string Ori2, string conf2, string External_2,
                                    string Spd, string Zone, string tool, string Wobj,bool IfVal)
            {
                string Point_C = Pt3D1 + "," + Ori1 + "," + Pt3D2 + "," + Ori2;
                string Configuration_C = conf1 + "," + conf2;
                string External_C = External_1 + "," + External_2;
                MoveCmd MoveC_Obj = new MoveCmd
                {
                    Id = ID,
                    MoveType = MOVE_TYPE.MOVEC,
                    Point = Point_C,
                    Configuration = Configuration_C,
                    External_Axis = External_C,
                    Speed = Spd,
                    Zone = Zone,
                    Tool = tool,
                    Wobj = Wobj,
                };
                Obj.moveCmds.Add(MoveC_Obj);
            }
        }
    }
}
