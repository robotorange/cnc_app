﻿/**
* @file      CommonFunction.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Apr 9, 2020
* @brief     Common function for ABB robot Rapid file parsing ; 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logger;

namespace CNC_APP.RapidProcess
{
    class CommonFunction
    {
        public void Str2Double(string str, out double[] val)
        {
            string[] strSplit = str.Split(',');
            val = new double[strSplit.GetLength(0)];
            for (int j = 0; j < strSplit.GetLength(0); j++)
            {
                if (double.TryParse(strSplit[j], out val[j]))
                { 
                  //  CLog.Info(val[j].ToString());
                }
                else
                {
                   // CLog.Info(string.Format("\nErrors: The string {0:G} can't cast to double type!", strSplit[j]));
                }
            }
        }
        public void Str2Int(string str, out int[] val)
        {
            string[] strSplit = str.Split(',');
            val = new int[strSplit.GetLength(0)];
            for (int j = 0; j < strSplit.GetLength(0); j++)
            {
                if (int.TryParse(strSplit[j], out val[j]))
                { 
                   // Console.WriteLine("{0:E3}", val[j]);
                }
                else
                { 
                    //Console.WriteLine("\nErrors: The string {0} can't cast to double type!", strSplit[j]);
                }
            }

        }
        public void Str2Substr(string str, out string[] substring)
        {
            substring = str.Split(',');
        }

        public void Substr2Str(string[] substring,out string str)
        {
            str = String.Join(",", substring);
        }

        public void Double2Str(double[] val,int num,out string str)
        {
            str = "";
            for (int i = 0; i < num; i++)
            {
                str += val[i].ToString()+",";
            }
            str = str.TrimEnd(',');
        }

        public void Int2Str(int[] val, int num, out string str)
        {
            str = "";
            for (int i = 0; i < num; i++)
            {
                str += val[i].ToString() + ",";
            }
            str = str.TrimEnd(',');
        }

        public void Pos2Pos3dAndOri(string str, out string[] Pos3D,out string[] ori)
        {
            string[] substring = new string[7];
            Pos3D = new string[3];
            ori = new string[4];
            substring = str.Split(',');
            for(int nIdx=0;nIdx<7;nIdx++)
            {
                if (nIdx < 3)
                {
                    Pos3D[nIdx] = substring[nIdx];
                }
                else
                {
                    ori[nIdx] = substring[nIdx];
                }     
            }
        }

        public void Pos3dAndOri2Pos(string[] Pos3D, string[] ori, out string PosObj)
        {
            PosObj = "";
            string wholePos3D = "";
            Substr2Str(Pos3D, out wholePos3D);
            string wholeOri = "";
            Substr2Str(ori, out wholeOri);
            PosObj = wholePos3D + "," + wholeOri;
        }

        public void Pos3dAndOri2Pos(double[] Pos3D, double[] ori, out string PosObj)
        {
            PosObj = "";
            string wholePos3D = "";
            Double2Str(Pos3D, 3,out wholePos3D);
            string wholeOri = "";
            Double2Str(ori,4, out wholeOri);
            PosObj = wholePos3D + "," + wholeOri;
        }
    }
}
