﻿/**
* @file      MoveCmdParser.cs
* @author    Xi Bao Shi(tigerxbs59@126.com)
* @date      Apr 9, 2020
* @brief     parsing the move command of ABB rapid file ; 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Logger;

namespace CNC_APP.RapidProcess
{
    class ToolCmdParser
    { 
        public void Tooldata(string ToolString, out double[] Point3d, out double[] Orientation)
        {
            CommonFunction CommFuc = new CommonFunction();
            Point3d = new double[3];
            Orientation = new double[4];
            string sToolName;
            Regex regex = new Regex("PERS tooldata");
            if (regex.IsMatch(ToolString))
            {
                ToolString = ToolString.Replace(@"PERS tooldata", string.Empty);
                ToolString = ToolString.Trim();
                CLog.Info(ToolString);
            }
            string[] results = Regex.Split(ToolString, @"\:\=");
            sToolName = results[0];
            string[] ToolParas = Regex.Split(results[1], @"\]\,");
            for (int i = 0; i < ToolParas.Length; i++)
            {
                if (0 == i)
                {
                    ToolParas[i] = ToolParas[i].Substring(ToolParas[i].IndexOf("[[") + 1);// trim"[["
                    ToolParas[i] = ToolParas[i].Trim('[');
                    CommFuc.Str2Double(ToolParas[i], out Point3d);
                }
                else if (1 == i)
                {
                    ToolParas[i] = ToolParas[i].TrimStart('[');
                    ToolParas[i] = ToolParas[i].TrimEnd(']');
                    CommFuc.Str2Double(ToolParas[i], out Orientation);
                }
                else
                {
                    // wait to updata;
                }
            }
        }
    }

    class WobjCmdParser
    {
        public void Wobjdata(string WobjString, out double[] Point3d, out double[] Orientation)
        {
            CommonFunction CommFuc = new CommonFunction();
            // int atPoint3d;
            Point3d = new double[3];
            Orientation = new double[4];
            string sWobjName;
            //  bool robhold, ufprog;
            Regex regex = new Regex("PERS wobjdata");
            if (regex.IsMatch(WobjString))
            {
                WobjString = WobjString.Replace(@"PERS wobjdata", string.Empty);
                WobjString = WobjString.Trim();
                CLog.Info(WobjString);
            }
            string[] results = Regex.Split(WobjString, @"\:\=");
            sWobjName = results[0];
            string[] WObjParas = Regex.Split(results[1], @"\]\,");
            for (int i = 0; i < WObjParas.Length; i++)
            {
                if (0 == i)
                {
                    WObjParas[i] = WObjParas[i].Substring(WObjParas[i].IndexOf("[[") + 1);// trim"[["
                    WObjParas[i] = WObjParas[i].Trim('[');
                    CommFuc.Str2Double(WObjParas[i], out Point3d);
                }
                else if (1 == i)
                {
                    WObjParas[i] = WObjParas[i].TrimStart('[');
                    WObjParas[i] = WObjParas[i].TrimEnd(']');
                    CommFuc.Str2Double(WObjParas[i], out Orientation);
                }
                else
                {
                    // wait to updata;
                }
            }
        }
    }

    class MoveCmdParser
    {
        public void ToolWobjPaser(string str, out List<string> substring)
        {
            string[] results = Regex.Split(str, @"\\");
            string Tool = results[0];
            string[] WobJs = Regex.Split(results[1], @"\:\=");
            string Wobj = WobJs[1];
            substring = new List<string>();
            substring.Add(Tool);
            substring.Add(Wobj);
        }

        public void MoveAbsJParser(string MoveAbsJString, out double[] Joints, out string[] External, out string sSpd,
                                    out string sZone, out string sTool)
        {
            CommonFunction CommFuc = new CommonFunction();
            Joints = new double[6];
            External = new string[6];
            sSpd = "";
            sZone = "";
            sTool = "";
            int atPoint6 = 0;
            int atExternal = 0;
            int atTail = 0;

            Regex regex = new Regex("MoveAbsJ");
            if (regex.IsMatch(MoveAbsJString))
            {
               
                MoveAbsJString = MoveAbsJString.Replace(@"MoveAbsJ", string.Empty);

                MoveAbsJString = MoveAbsJString.Trim();
                CLog.Info("MoveAbsJ parsing:"+ MoveAbsJString);
            }
            //    string pattern = @"\b(\w+)\[\[([+-]?)/d*[.]?/d*";
            string[] results = Regex.Split(MoveAbsJString, @"\]\,");
            for (int i = 0; i < results.Length; i++)
            {
                
                //1.判断是否有"[[",去掉[[,解析成6个值，去除
                if (((atPoint6 = results[i].IndexOf("[[")) != -1))
                {
                    results[i] = results[i].Substring(2);// trim"[["
                    CommFuc.Str2Double(results[i], out Joints);
                }

                //2.判断是否有"[",']',有就去掉，解析成6个值；
                else if (((atExternal = results[i].IndexOf("[")) != -1) && ((atExternal = results[i].IndexOf("]")) != -1))
                {
                    results[i] = results[i].Trim('[');
                    results[i] = results[i].Trim(']');// trim"[["
                    CommFuc.Str2Substr(results[i], out External);
                }

                //3.判断最后一个字符是不是;
                else if ((atTail = results[i].IndexOf(";")) != -1)
                {
                    results[i] = results[i].Trim(';');
                    string[] other = new string[3];
                    CommFuc.Str2Substr(results[i], out other);
                    sSpd = other[0];
                    sZone = other[1];
                    sTool = other[2];
                }
                else
                {
                    CLog.Error("parse MoveAbsJ error");
                }
            }
        }

        public void MoveJParser(string MoveJString, out double[] Point3d, out double[] Orientation, out int[] conf,
                                out string[] External, out double[] Spd4, out string sZone, out string sTool, out string sWobj)
        {
            CommonFunction CommFuc = new CommonFunction();
            Point3d = new double[3];
            Orientation = new double[4];
            conf = new int[4];
            External = new string[6];
            Spd4 = new double[4];
            sZone = "";
            sTool = "";
            sWobj = "";
            int atPoint3d = 0;
            int atTail = 0;

            Regex regex = new Regex("MoveJ");
            if (regex.IsMatch(MoveJString))
            {

                MoveJString = MoveJString.Replace(@"MoveJ", string.Empty);
                MoveJString = MoveJString.Trim();
                
                CLog.Info("MoveL Parsing:" + MoveJString);
            }
            //string pattern = @"\b(\w+)\[\[([+-]?)/d*[.]?/d*";
            string[] results = Regex.Split(MoveJString, @"\]\,");
            for (int i = 0; i < results.Length; i++)
            {
               
                //1.判断是否有"[[",去掉[[,解析成3个值，去除
                if (((atPoint3d = results[i].IndexOf("[[")) != -1))
                {
                    results[i] = results[i].Substring(2);// trim"[["
                    CommFuc.Str2Double(results[i], out Point3d);
                }

                //2.Orientation
                else if (1 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Orientation);
                }
                //3.configuration
                else if (2 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Int(results[i], out conf);
                }
                //4.external axis
                else if (3 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    results[i] = results[i].TrimEnd(']');
                    CommFuc.Str2Substr(results[i], out External);
                }
                //5.speed
                else if (4 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Spd4);
                }
                //6.判断最后一个字符是不是;
                else if ((atTail = results[i].IndexOf(";")) != -1)
                {
                    results[i] = results[i].Trim(';');
                    string[] other = new string[2];
                    CommFuc.Str2Substr(results[i], out other);
                    sZone = other[0];
                    List<string> ToolWobj;
                    ToolWobjPaser(other[1], out ToolWobj);
                    sTool = ToolWobj[0];
                    sWobj = ToolWobj[1];
                }
                else
                {
                    CLog.Error("parse MoveJ error");
                }
            }
        }

        public void MoveLParser(string MoveLString, out double[] Point3d, out double[] Orientation, out int[] conf,
                                out string[] External, out double[] Spd4, out string sZone, out string sTool, out string sWobj)
        {
            CommonFunction CommFuc = new CommonFunction();
            Point3d = new double[3];
            Orientation = new double[4];
            conf = new int[4];
            External = new string[6];
            Spd4 = new double[4];
            sZone = "";
            sTool = "";
            sWobj = "";
            int atPoint3d = 0;
            int atTail = 0;

            Regex regex = new Regex("MoveL");
            if (regex.IsMatch(MoveLString))
            {

                MoveLString = MoveLString.Replace(@"MoveL", string.Empty);
                MoveLString = MoveLString.Trim();
               
                CLog.Info("MoveL Parsing:" + MoveLString);
            }
            //    string pattern = @"\b(\w+)\[\[([+-]?)/d*[.]?/d*";
            string[] results = Regex.Split(MoveLString, @"\]\,");
            for (int i = 0; i < results.Length; i++)
            {
                //Console.WriteLine("{0}", results[i]);
                //1.判断是否有"[[",去掉[[,解析成3个值，去除
                if (((atPoint3d = results[i].IndexOf("[[")) != -1))
                {
                    results[i] = results[i].Substring(2);// trim"[["
                    CommFuc.Str2Double(results[i], out Point3d);
                }

                //2.Orientation
                else if (1 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Orientation);
                }
                //3.configuration
                else if (2 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Int(results[i], out conf);
                }
                //4.external axis
                else if (3 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    results[i] = results[i].TrimEnd(']');
                    CommFuc.Str2Substr(results[i], out External);
                }
                //5.speed
                else if (4 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Spd4);
                }
                //6.判断最后一个字符是不是;
                else if ((atTail = results[i].IndexOf(";")) != -1)
                {
                    results[i] = results[i].Trim(';');
                    string[] other = new string[2];
                    CommFuc.Str2Substr(results[i], out other);
                    sZone = other[0];
                    List<string> ToolWobj;
                    ToolWobjPaser(other[1], out ToolWobj);
                    sTool = ToolWobj[0];
                    sWobj = ToolWobj[1];
                }
                else
                {
                    CLog.Error("parse MoveL error");
                }
            }
        }

        public void MoveCParser(string MoveCString, out double[] Point3d_1, out double[] Orientation_1, out int[] conf_1, out string[] External_1,
                                 out double[] Point3d_2, out double[] Orientation_2, out int[] conf_2, out string[] External_2,
                                 out double[] Spd4, out string sZone, out string sTool, out string sWobj)
        {
            CommonFunction CommFuc = new CommonFunction();
            Point3d_1 = new double[3];
            Orientation_1 = new double[4];
            conf_1 = new int[4];
            External_1 = new string[6];

            Point3d_2 = new double[3];
            Orientation_2 = new double[4];
            conf_2 = new int[4];
            External_2 = new string[6];

            Spd4 = new double[4];
            sZone = "";
            sTool = "";
            sWobj = "";
            int atTail = 0;

            Regex regex = new Regex("MoveC");
            if (regex.IsMatch(MoveCString))
            {

                MoveCString = MoveCString.Replace(@"MoveC", string.Empty);
                MoveCString = MoveCString.Trim();                
                CLog.Info("MoveC Parsing:"+ MoveCString);
            }
            //    string pattern = @"\b(\w+)\[\[([+-]?)/d*[.]?/d*";
            string[] results = Regex.Split(MoveCString, @"\]\,");
            for (int i = 0; i < results.Length; i++)
            {
                //Console.WriteLine("{0}", results[i]);
                //1.get x,y,z trim"[["Point_1
                // if (((atPoint3d = results[i].IndexOf("[[")) != -1))
                if (0 == i)
                {
                    results[i] = results[i].Substring(2);// trim"[["
                    CommFuc.Str2Double(results[i], out Point3d_1);
                }
                //2.Orientation_1
                else if (1 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Orientation_1);
                }
                //3.configuration_1
                else if (2 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Int(results[i], out conf_1);
                }
                //4.external axis_1
                else if (3 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    results[i] = results[i].TrimEnd(']');
                    CommFuc.Str2Substr(results[i], out External_1);
                }
                //5.Point_2
                else if (4 == i)
                {
                    results[i] = results[i].Substring(2);
                    CommFuc.Str2Double(results[i], out Point3d_2);
                }
                //6.Orientation_2
                else if (5 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Orientation_2);
                }
                //7.configuration_2
                else if (6 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Int(results[i], out conf_2);
                }
                //8.external axis_2
                else if (7 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    results[i] = results[i].TrimEnd(']');
                    CommFuc.Str2Substr(results[i], out External_2);
                }
                //9.speed
                else if (8 == i)
                {
                    results[i] = results[i].TrimStart('[');
                    CommFuc.Str2Double(results[i], out Spd4);
                }

                //10.判断最后一个字符是不是;
                else if ((atTail = results[i].IndexOf(";")) != -1)
                {
                    results[i] = results[i].Trim(';');
                    string[] other = new string[2];
                    CommFuc.Str2Substr(results[i], out other);
                    sZone = other[0];
                    List<string> ToolWobj;
                    ToolWobjPaser(other[1], out ToolWobj);
                    sTool = ToolWobj[0];
                    sWobj = ToolWobj[1];
                }
                else
                {
                    CLog.Error("parse MoveC error");
                }
            }
        }

        

     
    }
}
